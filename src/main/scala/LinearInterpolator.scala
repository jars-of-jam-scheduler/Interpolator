/**
  * Linearly interpolates between values
  */
class LinearInterpolator {

  /* <!--------------------------------------------------------- BEGINNING - Interpolation -> */
  /**
    * Linearly interpolates a point between N points of D coordinates. The N points are in bijunction with the set of the weights.
    * This interpolation is linear.
    *
    * @param weights the set of the weights
    * @param points the set of the points
    * @return the linearly interpolated point of D coordinates, interpolated from the N points
    */
  def linearlyInterpolate(weights: Seq[Double], points: Seq[Seq[Double]], transform: (Double) => Double = null) : Seq[Double] = {
    var transformed_weights = weights
    if(transform != null) {
      transformed_weights = weights.map(transform)
    }
    if(Math.abs(transformed_weights.sum - 1.0) >= 0.0001) {
      println("ERROR : `SUM(weights) != 1`. Returning `null`.")
      return null
    }
    if(transformed_weights.exists(weight => BigDecimal(weight).setScale(5, BigDecimal.RoundingMode.HALF_UP).toDouble < 0)
      ||
      transformed_weights.exists(weight => BigDecimal(weight).setScale(5, BigDecimal.RoundingMode.HALF_UP).toDouble > 1)) {
      println("ERROR : `EXISTS(weight) / weight < -1 OR weight > 1`. Returning `null`.")
      return null
    }

    transformed_weights.zip(points).map(
      weight_point => weight_point._2.map(coordinate => weight_point._1 * coordinate)
    ).reduce((point_a : Seq[Double], point_b : Seq[Double]) => point_a.zip(point_b).map(coordinate_points => coordinate_points._1 + coordinate_points._2))
  }

  /**
    * Computes the interpolated point between four points.
    * Works by interpolating between the points of the first of the both pairs of points, then between the points of the
    * last ; finally, between each of the resulting both interpolated points.
    *
    * @param points the four points to interpolate between.
    * @param first_weight the delta between two consecutive values for a given weight. Used in the first both
    *                   interpolations.
    * @param last_weight the delta between two consecutive values for a given weight. Used in the last interpolation.
    * @param transform remaps each weight of an interpolated point. Possible transformations are : linear, cosine. Use
    *                  this parameter to modify the appearance of the interpolation.
    * @return the returned points are those that were interpolated from the both interpolated points.
    */
  def computeTheLinearlyInterpolatedPointBetween4Points(points : Seq[Seq[Double]], first_weight : Double, last_weight : Double, transform: (Double) => Double) : Seq[Double] = {
    val ret_1 = linearlyInterpolate(Seq(transform(1 - first_weight), transform(first_weight)), Seq(points.head, points(1)))
    val ret_2 = linearlyInterpolate(Seq(transform(1 - first_weight), transform(first_weight)), Seq(points(2), points.last))
    linearlyInterpolate(Seq(transform(1 - last_weight), transform(last_weight)), Seq(ret_1, ret_2))
  }
  /* <!--------------------------------------------------------- END - Interpolation -> */

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /* <!--------------------------------------------------------- BEGINNING - Computing all the interpolated points -> */
  /**
    * Builds (only) correct interpolated points, by computing a first pair of interpolated points and, then, by
    * interpolating the both later.
    *
    * @param points the four points to interpolate between.
    * @param first_step the delta between two consecutive values for a given weight. Used in the first both
    *                   interpolations.
    * @param last_step the delta between two consecutive values for a given weight. Used in the last interpolation.
    * @param transform remaps each weight of an interpolated point. Possible transformations are : linear, cosine. Use
    *                  this parameter to modify the appearance of the interpolation.
    * @return only the correct interpolated points are returned, i.e. those whose difference between 1 and the sum of
    *         their weights is strictly less than 0.0001. By the way, these returned points are those that were
    *         interpolated from the both interpolated points.
    */
  def computeAllPossibleLinearlyInterpolatedPointsBetween4PointsFast(points : Seq[Seq[Double]], first_step : Double, last_step : Double, transform :  (Double) => Double) : Seq[Seq[Double]] = {
    var returned_object : Seq[Seq[Double]] = Seq.empty[Seq[Double]]

    (.0 to 1.0 by transform(first_step)).foreach(step => {
      val ret_1 = linearlyInterpolate(Seq(1 - step, step), Seq(points.head, points(1)))
      val ret_2 = linearlyInterpolate(Seq(1 - step, step), Seq(points(2), points.last))

      (.0 to 1.0 by transform(last_step)).foreach(step => {
        returned_object = returned_object :+ linearlyInterpolate(Seq(1 - step, step), Seq(ret_1, ret_2))
      })

    })

    returned_object
  }

  /**
    * Builds correct and incorrect interpolated points by recursively iterating over their weights.
    * Works by interpolating between the points of the first of the both pairs of points, then between the points of the
    * last ; finally, between each of the resulting both interpolated points.
    *
    * @param first_step the delta between two consecutive values for a given weight. Used in the first both
    *                   interpolations.
    * @param last_step the delta between two consecutive values for a given weight. Used in the last interpolation.
    * @param points the four points to interpolate between.
    * @param transform remaps each weight of an interpolated point. Possible transformations are : linear, cosine. Use
    *                  this parameter to modify the appearance of the interpolation.
    * @param last_interpolation indicates if the current interpolation occurs between the both interpolated points
    * @return only the correct interpolated points are returned, i.e. those whose difference between 1 and the sum of
    *         their weights is strictly less than 0.0001. By the way, these returned points are those that were
    *         interpolated from the both interpolated points.
    */
  def computeAllPossibleLinearlyInterpolatedPointsBetween4Points(first_step : Double, last_step : Double, points : Seq[Seq[Double]], transform: (Double) => Double, last_interpolation: Boolean = false) : Seq[Seq[Double]] = {
    var returned_object : Seq[Seq[Double]] = Seq.empty[Seq[Double]]
    val used_step : Double = if(last_interpolation) last_step else first_step

    recursivelyLinearlyInterpolate(0, Seq.empty[Double])

    def recursivelyLinearlyInterpolate(current_weight_id : Int, building_weights : Seq[Double]) : Unit = {

      (.0 to 1.0 by transform(used_step)).foreach(current_step => {
        if (current_weight_id < points.size - 1) {
          recursivelyLinearlyInterpolate(current_weight_id + 1, building_weights :+ current_step)
        } else {
          val found_solution = building_weights :+ current_step

          if(Math.abs(found_solution.sum - 1.0) < 0.0001) {
            if (last_interpolation) {
              returned_object = returned_object :+ linearlyInterpolate(found_solution, points)
            } else {
              val ret_1 = linearlyInterpolate(found_solution, Seq(points.head, points(1)))
              val ret_2 = linearlyInterpolate(found_solution, Seq(points(2), points.last))
              computeAllPossibleLinearlyInterpolatedPointsBetween4Points(first_step, last_step, Seq(ret_1, ret_2), transform, last_interpolation = true).foreach(e => {
                returned_object = returned_object :+ e
              })
            }
          }
        }
      })
    }

    returned_object
  }

  /**
    * Builds correct and incorrect interpolated points by recursively iterating over their weights.
    *
    * @param step the delta between two consecutive values for a given weight
    * @param points the points to interpolate between
    * @param transform remaps each weight of an interpolated point. Possible transformations are : linear, cosine. Use
    *                  this parameter to modify the appearance of the interpolation.
    * @return only the correct interpolated points are returned, i.e. those whose difference between 1 and the sum of
    *         their weights is strictly less than 0.0001
    */
  def computeAllPossibleLinearlyInterpolatedPoints(step : Double, points : Seq[Seq[Double]], transform: (Double) => Double) : Seq[Seq[Double]] = {
    var returned_object : Seq[Seq[Double]] = Seq.empty[Seq[Double]]
    recursivelyLinearlyInterpolate(0, Seq.empty[Double])

    def recursivelyLinearlyInterpolate(current_weight_id : Int, building_weights : Seq[Double]) : Unit = {

      (.0 to 1.0 by transform(step)).foreach(current_step => {
        if (current_weight_id < points.size - 1) {
          recursivelyLinearlyInterpolate(current_weight_id + 1, building_weights :+ current_step)
        } else {
          val found_solution = building_weights :+ current_step
          if(Math.abs(found_solution.sum - 1.0) < 0.0001) {
            returned_object = returned_object :+ linearlyInterpolate(found_solution, points)
          }
        }
      })
    }

    returned_object
  }
  /* <!--------------------------------------------------------- END - Computing all the interpolated points -> */

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /* <!-------------------------------------------------------------------------------- BEGINNING - Transformations ->*/
  /**
    * One of the possible transformations. Just returns the weight.
    *
    * @param weight the weight to remap
    * @return the weight remapped as the weight
    */
  def linear(weight : Double) : Double = {
    weight
  }

  /**
    * One of the possible transformations. Returns (1 - Math.cos(weight * Math.PI)) * 0.5.
    *
    * @param weight the weight to remap
    * @return (1 - Math.cos(weight * Math.PI)) * 0.5
    */
  def cosine(weight : Double) : Double = {
    (1 - Math.cos(weight * Math.PI)) * 0.5
  }
  /* <!-------------------------------------------------------------------------------- END - Transformations ->*/

}
object Main {
  def main(args : Array[String]) : Unit = {

    val linear_interpolator = new LinearInterpolator
    val points = Seq(Seq(0.3), Seq(0.8), Seq(0.4), Seq(1.0))
    val res = linear_interpolator.computeAllPossibleLinearlyInterpolatedPointsBetween4PointsFast(points, 0.1, 0.1, linear_interpolator.linear)
    println(res.mkString("\n"))

  }
}
